[`pet3rmatta/msoe-se3800-cucumber`](https://hub.docker.com/r/pet3rmatta/msoe-se3800-cucumber/)
==============================================================================================

Environment for SE3800 exercises at [MSOE](https://www.msoe.edu) requiring
[`cucumber`](https://cucumber.io) bindings for Java.

Build:
------

```bash
docker build --file ./<tag>/Dockerfile --tag pet3rmatta/msoe-se3800-cucumber:<tag> .
```

Supported Tags:
---------------

- `ubuntu`
- `alpine`

Usage:
------

```bash
cd /path/to/your/directory
docker run --rm -it -v $(pwd):/usr/msoe/cucumber pet3rmatta/msoe-se3800-cucumber
run-cucumber
```

Start docker container mounting your current directory as the container workspace directory,
from where you can run (modified) `run-cucumber` script from the
[`selling-pyramids`](https://faculty-web.msoe.edu/hasker/se3800/samples/)
examples to compiler and run the program.

